﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lec1_LINQ_Collections.Models.Interface
{
    /// <summary>
    /// Інтерфейс стану меню
    /// </summary>
    public interface IState
    {
        IState RunState();
    }
}