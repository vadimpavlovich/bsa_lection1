﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lec1_LINQ_Collections.Models.DTO
{
    public class UserTasksFinishedDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
