﻿using Lec1_LINQ_Collections.Models.API; 
using System.Collections.Generic; 

namespace Lec1_LINQ_Collections.Models.DTO
{
    public class UsersWithTasksDTO : Users
    {
        public List<Tasks> Tasks { get; set; }
    }
}
