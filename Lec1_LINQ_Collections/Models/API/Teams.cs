﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic; 

namespace Lec1_LINQ_Collections.Models.API
{
    public class Teams
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        public IEnumerable<Users> Users { get; set; }
    }
}
