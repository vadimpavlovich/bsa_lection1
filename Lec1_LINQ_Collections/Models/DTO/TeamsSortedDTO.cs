﻿using Lec1_LINQ_Collections.Models.API;
using System.Collections.Generic;

namespace Lec1_LINQ_Collections.Models.DTO
{
    public class TeamsSortedDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Users> Users { get; set; }
    }
}
