﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic; 

namespace Lec1_LINQ_Collections.Models.API
{
    public class Projects
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }


        public IEnumerable<Tasks> Tasks { get; set; } 
        public Teams Team { get; set; } 
        public Users Author { get; set; } 
    }

}
