﻿using Newtonsoft.Json;
using System;

namespace Lec1_LINQ_Collections.Models.API
{
    public class Users
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("teamId")]
        public int? TeamId { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("registeredAt")]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("birthDay")]
        public DateTime BirthDay { get; set; }
    }
}
