﻿ 
namespace Lec1_LINQ_Collections.Models.API
{ 
    public enum TaskSatete
    {
        Created = 0,
        InProgress = 1,
        Finished = 2,
        Cancelled = 3
    }
}
