﻿using Lec1_LINQ_Collections.Models.Interface;
using Lec1_LINQ_Collections.Services;
using System;

namespace Lec1_LINQ_Collections
{
    class Program
    {
        public static MainService service { get; set; }
        static void Main(string[] args)
        {
            service = new MainService();
            Console.WriteLine($"Привіт, {Environment.UserName}!");
            Console.WriteLine($"Дані завантажуються, будь-ласка зачекайте."); 
            try
            {
                service.LoadData(); 
            }
            catch
            {
                Console.WriteLine("При завантаженні данних виникла помилка. Будь-ласка спробуйте пізніше.");
                Console.WriteLine("Натисніть Enter для виходу.");
                Environment.Exit(0);
            }
            IState startState = new MainMenu();
            while (startState != null) startState = startState.RunState();
            Console.Clear();
            Console.WriteLine($"До зустрічі, {Environment.UserName}!");
        }
    }
}
